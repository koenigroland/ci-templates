# ci-templates

Central place to maintain ci settings.

## Usage

You can use the project templates by including them in your ci file.

```yaml
include:
  - project: koenigroland/ci-templates
    ref: main
    file: maven.gitlab-ci.yml